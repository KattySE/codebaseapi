﻿using CodeBaseAPI.Domain;
using System.Threading.Tasks;

namespace CodeBaseAPI.Interfaces
{
    public interface ICodeService
    {
        Task<Code> ProcessCode(Code code);
    }
}
