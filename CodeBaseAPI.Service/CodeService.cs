﻿using System;
using CodeBaseAPI.Domain;
using CodeBaseAPI.Interfaces;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace CodeBaseAPI.Service
{
    public class CodeService : ICodeService
    {
        string standardError;
        string standardOutput;
        string globalError;

        public CodeService()
        {
        }

        public async Task<Code> ProcessCode(Code code)
        {
            var response = code;
            var copyCodeResponse = true;
            await Task.Run(() =>
            {
                try
                {
                    if (copyCodeResponse) copyCodeResponse &= CopyCode(code);
                    if (copyCodeResponse) copyCodeResponse &= CopyCodeToContainerAsync(code).Result;
                    if (copyCodeResponse) copyCodeResponse &= BuildContainerCodeAsync(code).Result;
                    if (copyCodeResponse) copyCodeResponse &= ExecContainerCodeAsync(code, "FS-Bootcamp").Result;
                    if (copyCodeResponse) copyCodeResponse &= ReadOutPutFile(code, standardOutput).Result;
                }
                catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                    code.Error = globalError;
                    copyCodeResponse = false;
                }
                finally {
                    if (copyCodeResponse)
                    {
                        code.Response = "Success";
                        code.Output = standardOutput;
                    }
                    else
                    {
                        code.Response = "Error";
                    }
                    code.ErrorDetail = standardError;
                }

            });
            return response;
        }

        private bool CopyCode(Code code)
        {
            bool response = true;
            try
            {
                var bytes = Encoding.UTF8.GetBytes(code.Text);
                using (var f = File.Open(@"C:\Users\kater\Documents\GitLab\rust_hello_world\src\main.rs", FileMode.Create))
                {
                    f.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = false;
            }
            return response;
        }

        private async Task<bool> CopyCodeToContainerAsync(Code code)
        {
            var response = false;
            var cmd = "docker cp C:\\Users\\kater\\Documents\\GitLab\\rust_hello_world\\src\\main.rs rust-container:/usr/src/myapp/src/main.rs";
            try
            {
                response = await Bash(cmd, true) == 0;
            }
            catch (Exception ex)
            {
                globalError = "Error copying the code to the compiler.";
                throw ex;
            }
            return response;
        }

        private async Task<bool> BuildContainerCodeAsync(Code code)
        {
            var response = false;
            var cmd = "docker exec rust-container cargo install --path .";
            try
            {
                response = await Bash(cmd, true) == 0;
            }
            catch (Exception ex)
            {
                globalError = "Error compiling the code.";
                throw ex;
            }
            return response;
        }

        private async Task<bool> ExecContainerCodeAsync(Code code, string arg)
        {
            var fileLocation = DateTime.Now.ToFileTime().ToString() + ".txt";
            var response = false;
            var cmd = $"docker exec rust-container /bin/bash -c \"/usr/src/myapp/run.sh {arg} {fileLocation}\""; //WARNING => Do not use the -it whit Docker commands, this cause troubles at execution time
            try
            {
                response = await Bash(cmd, true) == 0;
                standardOutput = fileLocation;
            }
            catch (Exception ex)
            {
                globalError = "Error executing the code.";
                throw ex;
            }
            return response;
        }

        private async Task<bool> ReadOutPutFile(Code code, string fileName)
        {
            bool response = true;
            var cmd = $"docker exec rust-container /bin/bash -c \"cat /output/{fileName}\"";
            try
            {
                response = await Bash(cmd, true) == 0;
            }
            catch (Exception ex)
            {
                globalError = "Error reading the output";
                Debug.WriteLine(ex.Message);
                standardError = ex.Message;
                throw ex;
            }
            return response;
        }

        public Task<int> Bash(string cmd, bool copyGlobals = false)
        {
            var source = new TaskCompletionSource<int>();
            var escapedArgs = cmd.Replace("\"", "\\\"");
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = @"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe", /*For powershell: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe | For cmd: C:\WINDOWS\system32\cmd.exe*/
                    Arguments = $"-c \"{escapedArgs}\"", /* For powershell: -c \"{escapedArgs}\", For cmd: /c \"{escapedArgs}\"*/
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = false
                },
                EnableRaisingEvents = true
            };

            process.Exited += (sender, args) =>
            {
                if (copyGlobals) {
                    standardError = process.StandardError.ReadToEnd();
                    standardOutput = process.StandardOutput.ReadToEnd();
                }

                Debug.WriteLine($"StandardError -> {process.StandardError.ReadToEnd()}");
                Debug.WriteLine($"StandardOutput -> {process.StandardOutput.ReadToEnd()}");

                if (process.ExitCode == 0)
                    source.SetResult(0);
                else
                    source.SetException(new Exception($"Command `{cmd}` failed with exit code `{process.ExitCode}`"));
                process.Dispose();
            };

            try
            {
                process.Start();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Command {cmd} failed");
                source.SetException(e);
            }

            return source.Task;
        }

    }
}
