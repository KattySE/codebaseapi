﻿using System;

namespace CodeBaseAPI.Domain
{
    public class Code
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Response { get; set; }
        public string Output { get; set; }
        public string Error { get; set; }
        public string ErrorDetail { get; set; }
    }
}
